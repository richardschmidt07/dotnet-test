﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str) {
            var stringToCharArray = str.ToCharArray();

            Array.Reverse(stringToCharArray);

            return new String(stringToCharArray);
        }

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 1; i <= exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node) 
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append(node.Text + "-");

            var tree = (Tree)node;

            foreach(var outer in tree.Children) 
            {
                stringBuilder.Append(outer.Text + "-");

                var outerTree = (Tree)outer;

                foreach(var inner in outerTree.Children) 
                {
                    stringBuilder.Append(inner.Text + "-");
                    try 
                    {
                        var innerTree = (Tree)inner;

                        foreach (var leaf in innerTree.Children)
                        {
                            stringBuilder.Append(leaf.Text + "-");
                        }
                    }   
                    catch
                    {
                        continue;
                    }
                }
            }

            var returnString = stringBuilder.ToString();

            return returnString.Remove(returnString.Length - 1);
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
